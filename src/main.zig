const std = @import("std");
const Person = @import("person.zig").Person;
const parser = @import("parser.zig");
const CustomElement = @import("parser.zig").CustomElement;
// const Document = @import("parser.zig").Document;
const Text = @import("./elements/text.zig").Text;
const Image = @import("./elements/image.zig").Image;
const print = std.debug.print;

const test_allocator = std.testing.allocator;

pub const Document = struct {
    elements: []const CustomElement,

    pub fn register(elements: []const CustomElement) Document {
        return Document{
            .elements = elements,
        };
    }
};

pub fn main() !void {
    const doc = Document.register(&[_]CustomElement{
        CustomElement.new(Text, "text"),
        CustomElement.new(Image, "img"),
    });

    for (doc.elements) |item| {
        print("element {} \n", .{item.name});
    }

    // var doc = Document.sized(5);
    // doc.register(CustomElement.new(Text, "text"));
}
