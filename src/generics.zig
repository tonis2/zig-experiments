const std = @import("std");
const Person = @import("person.zig").Person;

pub fn add2(input: anytype) @TypeOf(input) {
    return input + @as(@TypeOf(input), 2);
}

pub fn run() void {
    var person1 = Person{ .name = "test" };

    const age: i32 = add2(person1.age);

    std.debug.print("Age {}", .{age});
}
