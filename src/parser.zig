const std = @import("std");
const test_allocator = std.testing.allocator;

pub fn zsx(string: []const u8) !void {
    var tokens = std.mem.tokenize(string, "<");
    if (tokens.rest().len > 0) {
        var tag_open = tokens.next().?;
        var tag = std.mem.split(tag_open, " ").next();
        std.debug.print("tag {} \n", .{tag});
    }
    // while (tokens.rest().len > 0) {

    //     std.debug.print("token {} \n", .{tokens.next().?});
    // }
}

pub const CustomElement = struct {
    comptime tag: anytype = u8,
    name: []const u8,

    pub fn new(comptime T: anytype, name: []const u8) CustomElement {
        return CustomElement{
            .tag = T,
            .name = name,
        };
    }
};

pub const Document = struct {
    elements: []const CustomElement,

    pub fn register(elements: []const CustomElement) Document {
        return Document{
            .elements = elements,
        };
    }
};
