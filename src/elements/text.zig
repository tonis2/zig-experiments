pub const Text = struct {
    content: []const u8,

    pub fn new(value: []const u8) Text {
        return Text{
            .content = value,
        };
    }
};
