pub const Image = struct {
    src: []const u8,
    width: u16,

    pub fn new(value: []const u8) Image {
        return Image{
            .src = value,
            .width = 0,
        };
    }

    pub fn width(self: *Image, value: u16) void {
        self.width = value;
    }
};
